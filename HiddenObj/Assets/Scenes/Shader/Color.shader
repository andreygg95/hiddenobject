Shader "Siluette/Color" {
	
	Properties {
	_MainTex ("Base (RGB) Alpha (A)", 2D) = "white" {}
	_Color ("Color", Color) = (1,1,1,1)
	}
	
	SubShader
	{
		Tags {"RenderType"="Transparent" "Queue"="Transparent"}
		LOD 200	
		Lighting Off	
		Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
		ZWrite Off
		Cull Off
		Fog { Mode Off }	
		
		Pass
		{			
			CGPROGRAM
			#pragma vertex vertexColor
			#pragma fragment fragmentColor
			#include "UnityCG.cginc"

			fixed4 _Color;	
			sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform float4 _MainTex_TexelSize;
						
			struct vertex_input 
			{ 
			float4 vertex : POSITION; 
			float2 texcoord0 : TEXCOORD0;
			};
			
			struct vertex_output 
			{
			 float4 vertex : SV_POSITION; 
			 float2 texcoord0 : TEXCOORD0;
			 };	
			
			vertex_output vertexColor(vertex_input v) 
			{ 
			 vertex_output o;
			 o.vertex = UnityObjectToClipPos(v.vertex);
			 o.texcoord0 = TRANSFORM_TEX(v.texcoord0, _MainTex);
			 return o; 
			 }

			half4 fragmentColor(vertex_output i) : COLOR 
			{ 
			_Color.a *= tex2D(_MainTex, i.texcoord0).a;
			return _Color;
			}				
			ENDCG
        }
	}
}
