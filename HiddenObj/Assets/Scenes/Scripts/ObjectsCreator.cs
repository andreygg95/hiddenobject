﻿using System;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class ObjectsCreator : MonoBehaviour
{
    public Camera MainCamera;
    public Transform siluetteParent;
    public Transform fieldParent;
    
    public GameObject[] examples;
    public Color SiluetteColor;
    public int Count;

    private Vector2 zeroFieldPos;
    private Vector2 fieldSize;
    
    public void Start()
    {
        var bounds = fieldParent.GetComponent<Renderer>().bounds;
        var exampleSize = examples[0].GetComponent<Renderer>().bounds.size;
        zeroFieldPos = bounds.min + exampleSize / 2;
        fieldSize = bounds.size - exampleSize;

        float width = MainCamera.orthographicSize * MainCamera.aspect * 2;
        for (int i = 0; i < Count; i++)
        {
            float xpos = -width / 2 + width * (i+.5f) / Count;
            CreatePair(examples[i % examples.Length], xpos);
        }
    }

    public void CreatePair(GameObject example, float xPos)
    {
        var obj = GameObject.Instantiate(example);
        obj.name = example.name;
        Vector3 pos = zeroFieldPos + new Vector2(fieldSize.x * Random.value, fieldSize.y * Random.value);
        pos.z = -2f;
        obj.transform.position = pos;
        obj.transform.parent = fieldParent;
        AddMesh(obj, 96, 96);

        var siluette = GameObject.Instantiate(example);
        siluette.name = example.name + "_Siluette";
        siluette.GetComponent<Renderer>().sharedMaterial = SiluetteMaterial(obj);
        siluette.GetComponent<MeshFilter>().sharedMesh = obj.GetComponent<MeshFilter>().sharedMesh;

        siluette.transform.position = new Vector3(xPos, siluetteParent.transform.position.y, -1f);

        AddText(siluette).text = obj.name;
        
        obj.AddComponent<ObjectBehaviour>().ShadowLink = siluette;
        obj.AddComponent<BoxCollider>();
    }

    private TextMesh AddText(GameObject siluette)
    {
        var go = new GameObject("text");
        var text = go.AddComponent<TextMesh>();
        text.transform.localScale = Vector3.one * 30f;
        text.anchor = TextAnchor.MiddleCenter;
        text.color = Color.black;
        go.transform.parent = siluette.transform;
        go.transform.localPosition = Vector3.down * 70f;
        return text;
    }
    
    private static void AddMesh(GameObject go, float x, float y)
    {
        var uv = new[] {Vector2.zero, Vector2.up, Vector2.one, Vector2.right,};
        var vert = uv.Select(item => new Vector3(-x / 2 + x * item.x, -y / 2 + y * item.y)).ToArray();
        go.GetComponent<MeshFilter>().sharedMesh = new Mesh
        {
            vertices = vert,
            uv = uv,
            triangles = new[] {0, 1, 2, 0, 2, 3}
        };
    }
    
    public Material SiluetteMaterial(GameObject go) =>
        SiluetteMaterial(go.GetComponent<Renderer>().sharedMaterial.mainTexture);
    
    public Material SiluetteMaterial(Texture tex) => 
        new Material(Shader.Find("Siluette/Color")) {mainTexture = tex, color = SiluetteColor};
}