﻿using UnityEngine;

public class StartButton : MonoBehaviour
{
    public GameObject menu;
    public GameObject game;
    private void OnMouseUpAsButton()
    {
        menu.SetActive(false);
        game.SetActive(true);
    }
}