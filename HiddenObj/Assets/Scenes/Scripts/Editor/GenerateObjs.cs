﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class GenerateObjs : MonoBehaviour
{
    [MenuItem("Tools/CreateObjs")]
    public static void CreateAssets()
    {
        foreach (var tex in Selection.GetFiltered<Texture>(SelectionMode.DeepAssets))
        {
            float x = tex.width;
            float y = tex.height;
            var go = new GameObject(tex.name);
            var mat = new Material(Shader.Find("Unlit/Transparent"));
            mat.mainTexture = tex;
            go.AddComponent<MeshRenderer>().sharedMaterial = mat;
            AddMesh(go, x, y);
        }
    }

    private static void AddMesh(GameObject go, float x, float y)
    {
        var uv = new[] {Vector2.zero, Vector2.up, Vector2.one, Vector2.right,};
        var vert = uv.Select(item => new Vector3(-x / 2 + x * item.x, -y / 2 + y * item.y)).ToArray();
        go.AddComponent<MeshFilter>().sharedMesh = new Mesh
        {
            vertices = vert,
            uv = uv,
            triangles = new[] {0, 1, 2, 0, 2, 3}
        };
    }

    [MenuItem("Tools/GenerateTex")]
    public static void GenerateTex()
    {
        int w = 129;
        var tex = new Texture2D(w, w);
        
        for(int i = 0; i < w; i++)
        for (int j = 0; j < w; j++)
        {
            float a = Mathf.Sqrt(Mathf.Pow(i - 64, 2) + Mathf.Pow(j - 64, 2))/64f;
            a = a * 2 - 1;
            tex.SetPixel(i,j,new Color(1,1,1,a));
        }

        tex.wrapMode = TextureWrapMode.Clamp;
        tex.Apply();
        
        Selection.activeTransform.GetComponent<Renderer>().sharedMaterial.mainTexture = tex;
    }
}
