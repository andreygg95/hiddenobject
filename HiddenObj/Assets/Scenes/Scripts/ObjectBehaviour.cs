﻿using System.Collections;
using UnityEngine;

public class ObjectBehaviour : MonoBehaviour
{
    public GameObject ShadowLink;

    private float flyTime = .7f;
    public void OnMouseUpAsButton()
    {
        GetComponent<BoxCollider>().enabled = false;
        StartCoroutine(MoveToShadow());
        WinCounter.instance.ObjectFinded();
    }

    private IEnumerator MoveToShadow()
    {
        float time = Time.time;
        var pos = transform.position;
        var posTo = ShadowLink.transform.position;
        
        var material = ShadowLink.GetComponent<MeshRenderer>().sharedMaterial;
        var textMesh = ShadowLink.transform.GetChild(0).GetComponent<TextMesh>();

        StartCoroutine(HideMaterial(material, flyTime));
        StartCoroutine(HideMaterial(textMesh, flyTime));

        while (Time.time < time + flyTime)
        {
            float arg = (Time.time - time) / flyTime;
            transform.position = Vector3.Lerp(pos,posTo, arg);
            transform.rotation = Quaternion.Euler(Vector3.back * 720 * arg);
            yield return new WaitForEndOfFrame();
        }

        transform.position = posTo;
        transform.rotation = Quaternion.identity;
        
        Destroy(ShadowLink);
    }

    private IEnumerator HideMaterial(Material material, float hideTime)
    {
        var color = material.color;
        float startA = color.a;
        float t = 0f;
        while (t < hideTime)
        {
            t += Time.deltaTime;
            color.a = startA * (1f - t / hideTime);
            material.color = color;
            yield return  new WaitForEndOfFrame();
        }
    }
    
    private IEnumerator HideMaterial(TextMesh text, float hideTime)
    {
        var color = text.color;
        float startA = color.a;
        float t = 0f;
        while (t < hideTime)
        {
            t += Time.deltaTime;
            color.a = startA * (1f - t / hideTime);
            text.color = color;
            yield return  new WaitForEndOfFrame();
        }
    }
}