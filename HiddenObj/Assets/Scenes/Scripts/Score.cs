﻿using System;
using UnityEngine;

public class Score : MonoBehaviour
{
    void Start() => GetComponent<TextMesh>().text = $"Score: {PlayerPrefs.GetInt("Score", 0)}";
}