﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class BackButton : MonoBehaviour
{
    public Camera MainCamera;
    public float Anchor;
    public bool checkTransform;
    
    public void Start()
    {
        if(!checkTransform) return;
        
        var parentScale = transform.parent.lossyScale;
        transform.localScale = new Vector3(1f / parentScale.x, 1f / parentScale.y, 1f);
        var size = GetComponent<Renderer>().bounds.size;
        var xpos = (size.x / 2 - MainCamera.orthographicSize * MainCamera.aspect + Anchor) / parentScale.x;
        transform.localPosition = new Vector3(xpos, 0, -1f);
    }

    public void OnMouseUpAsButton() => SceneManager.LoadScene(0);
}
