﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneBuilder : MonoBehaviour
{
    public Camera MainCamera;
    public GameObject[] TopSceneObjects;
    public GameObject CenterObject;
    public GameObject[] BottomSceneObjects;

    public void Awake()
    {
        var centerBounds = GetBounds(CenterObject);
        float width = centerBounds.size.x;
        
        var orthographicSize = width / MainCamera.aspect / 2;
        MainCamera.orthographicSize = orthographicSize;
        
        float targetSumHeight = orthographicSize * 2;
        targetSumHeight -= centerBounds.size.y;
        float ySize = targetSumHeight / (SumHeight(TopSceneObjects) + SumHeight(BottomSceneObjects));
        float startHeight = MainCamera.transform.position.y + orthographicSize;
        
        SetSizeAndPos(ref startHeight, ySize, width, TopSceneObjects);
        SetSizeAndPos(ref startHeight, 1f, width, CenterObject);
        SetSizeAndPos(ref startHeight, ySize, width, BottomSceneObjects);
    }

    private float SumHeight(GameObject[] objects)
    {
        float sum = 0;
        foreach (GameObject obj in objects)
            sum += GetBounds(obj).size.y;
        return sum;
    }

    private void SetSizeAndPos(ref float startHeight, float ySize, float width, params GameObject[] objects)
    {
        foreach (var obj in objects)
        {
            float xSize = width / GetBounds(obj).size.x;
            obj.transform.localScale = new Vector3(xSize, ySize, 1);
            float objHeight = GetBounds(obj).size.y;
            obj.transform.position = Vector3.up * (startHeight - objHeight / 2);
            startHeight -= objHeight;
        }
    }

    private Bounds GetBounds(GameObject obj) => obj.GetComponent<Renderer>().bounds;
}