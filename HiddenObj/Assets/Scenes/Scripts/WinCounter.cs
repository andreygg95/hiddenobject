﻿using System;
using System.Collections;
using UnityEngine;

public class WinCounter : MonoBehaviour
{
    public int NeededCount;
    public int FindedCount;
    public GameObject WinMenu;
    public static WinCounter instance;
    
    private void Awake() => instance = this;

    public void ObjectFinded()
    {
        FindedCount++;
        if (FindedCount == NeededCount)
            Win();
    }
    private void Win()
    {
        PlayerPrefs.SetInt("Score", PlayerPrefs.GetInt("Score", 0) + 1);
        StartCoroutine(AsyncWin());
    }

    private IEnumerator AsyncWin()
    {
        Timer.instance.Stop();
        yield return new WaitForSeconds(.7f);
        WinMenu.SetActive(true);
    }
}