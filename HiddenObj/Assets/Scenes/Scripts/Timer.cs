﻿using System;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public float time;
    public float size;
    public TextMesh text;
    public static Timer instance;
    private bool stopped = false;
    public GameObject loseMenu;

    public void Awake() => instance = this;

    public void Start()
    {
        var parentScale = transform.parent.lossyScale;
        transform.localScale = new Vector3(size / parentScale.x, size / parentScale.y, 1f);
    }

    public void Update()
    {
        if (stopped) return;

        time -= Time.deltaTime;
        text.text = $"00:{(int) time}";
        
        if(time < 0) Lose();
    }

    public void Stop() => stopped = true;
    
    private void Lose()
    {
        loseMenu.SetActive(true);
    }
}