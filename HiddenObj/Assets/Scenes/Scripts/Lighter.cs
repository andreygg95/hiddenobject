﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lighter : MonoBehaviour
{
    public Camera MainCamera;
    private Vector2 min;
    private Vector2 size;
    private Material mat;
    private void Awake()
    {
        var renderer = GetComponent<MeshRenderer>();
        var bounds = renderer.bounds;
        min = bounds.min;
        size = bounds.size;
        size = new Vector2(1f / size.x, 1f / size.y);
        mat = renderer.sharedMaterial;
    }

    void Update()
    {
        var mp = (Vector2)MainCamera.ScreenToWorldPoint(Input.mousePosition);
        var uv = Vector2.Scale(mp - min, size);
        uv = Vector2.Scale(uv,mat.mainTextureScale) - Vector2.one/2;
        mat.mainTextureOffset = -uv;
    }
}
